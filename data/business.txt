http://avc.com/2017/03/using-debt-like-growth-equity/
business
If you are in the venture or startup business and don’t read Dan Primack, consider changing that. He’s great.

From his newsletter this morning:

Indebted: Last week we noted that Wal-Mart subsidiary Jet.com had acquired ModCloth, an online retailer of vintage women’s apparel. No financial terms were disclosed, but this didn’t feel like a success for either ModCloth or the venture capitalists who had invested over $70 million into the business since its founding 15 years earlier. Here’s what happened, per sources familiar with the situation:

In 2013 ModCloth went out in search of Series C funding, but the process was felled by a back-to-back pair of lousy quarters. So instead it accepted $20 million in unsecured bank debt.

ModCloth effectively treated the debt like growth equity, rather than recognizing the time bomb it could become.

When the debt first came due in April 2015, existing ModCloth investors pumped in new equity to, in part, kick repayment down the road for two years. This came amid four to five straight quarters of profitability, and just after the company brought in a former Urban Outfitters executive as CEO.

Once the income statement returned to the red, ModCloth again tried raising equity ― but prospective investors cited the debt overhang as their reason for passing on a company whose unit economics were otherwise fundable. Insiders could have stepped up but didn’t.

Jet.com heard of ModCloth’s debt coming due debt month, and pounced. We’ve been unable to learn the exact amount it paid, except that the amount left over for VCs after repaying the debt (and accounting for receivables) won’t be nearly enough to make them whole.

2 takeaways: (1) Debt is not inherently troublesome for startups, particularly if it’s supplementing equity as opposed to substituting for equity. But startups must recognize that not all cash is created equal. (2) ModCloth was founded in Pittsburgh, but later moved its HQ to San Francisco. It’s impossible to know if things would have worked out differently had the company remained in the Steel City, but some of its quirky retail culture did seem to get commingled with the “grow grow” tech etho

I have lived this story several times in my career and we are seeing this play out again in the market.

It is tempting to use debt instead of equity to finance a high growth company, particularly when you cannot get equity investors to value your company “fairly.” When a company has achieved “escape velocity” and is growing quickly, lenders look at it and say “there is enterprise/takeout value here and we are senior to the equity so the risk to us is pretty low.” And so they will underwrite a loan to the company even though the market hasn’t made up its mind on how to properly value the equity. So the temptation all around the table is to take the debt and kick the can down the road on the equity in the view that more time, more growth, more market validation will fix things.

This can work out well. Our portfolio company Foursquare is an example of where this did work out well. A debt deal in the middle of a business model pivot gave that company the time to re-engineer its business model and validate it. And time also allowed the company to come to terms with how the equity markets would value it and its new business model. Foursquare went on to raise another round of equity capital and refinance its debt and is in a great place now.

But, as the Modcloth story points out, debt can also work against you. If you can’t execute well post raising debt and get to another equity round or some other transaction (an attractive exit being the other obvious option), then you can have your debt called from under you and lose the control over the timing and terms of your exit. I lived through this story with a company I backed in 1999 and which was sold a few years ago in a transaction that was very good for the lenders and good for the management and very bad for the early equity investors.

Dan’s point that substituting debt for growth equity is a risky bet is spot on. That doesn’t mean it shouldn’t be done. But it should be done with care and with eyes wide open.
---
http://www.businessinsider.com/dead-zones-threaten-coral-reefs-worldwide-ocean-smithsonian-tropical-research-institute-2017-3?utm_source=feedly&amp;utm_medium=referral
business
Scientists led by the Smithsonian Tropical Research Institute have discovered dead zones in a place where they least expected them: the Caribbean. More specifically, the tropics.

Dead zones are low-oxygen regions in the ocean where few sea life can survive. The main cause is human pollution. Until now, dead zones were thought to dominate mostly temperate zones on Earth, but the new study reveals that there are far more dead zones in the ocean than we thought.

The study also changes our outlook on the major threats to coral reefs. Before, warming oceans and acidification were the main concerns. But now, scientists must also consider dead zones as a major threat. Luckily, dead zones can be controlled relatively easily, unlike warming oceans and acidification, which would require an end to climate change.
---
http://www.calculatedriskblog.com/2017/03/housing-upside-and-downside-risks.html
business
In a note today, Merrill Lynch economist Michelle Meyer notes a few upside and downside risks for housing. A few excerpts:



The housing market is being hit by several cross currents. On the upside, the warmer than-normal weather in the winter likely boosted housing activity over the past few months. The risk, however, is that this could be pulling activity forward from the spring. In addition, the general improvement in the economy and gain in consumer confidence could be underpinning housing activity. The NAHB homebuilder confidence index has climbed higher, reaching a new cyclical high of 71 in March. Clearly builders are optimistic. However, on the downside , interest rates have increased which weighs on affordability .



There are also a variety of potential policy changes which can impact the outlook for the housing market. High on the list is financial market deregulation and its impact on the flow of credit. In addition, there seems to be renewed focus on reforming the mortgage finance system and bringing Fannie Mae and Freddie Mac out of conservatorship . In addition, immigration reform could have significant impacts on the housing market over the medium term .

emphasis added

CR note: If, later this year, the Fed starts to reduce their balance sheet, that might push up longer rates (and pushing up mortgage rates a little more). Another downside risk for housing is reduced foreign buying due to the strong dollar, U.S. political concerns, and capital controls in China.
---
https://www.entrepreneur.com/article/290878
business
Happiness stems from all aspects of life, from employment to health to friendships. While the building blocks of a satisfying life are universal, happiness levels vary from country to country.

The 2017 World Happiness Report by the UN Sustainable Development Solutions Network reveals which countries are the happiest, ranking 155 countries globally. To measure happiness levels worldwide, the report weighs six key variables: income; healthy life expectancy; social support (having someone to count on in times of trouble); generosity; freedom; and trust.

Related: You'll Never Believe Who the Happiest Employees in Europe Are

The same 10 countries have topped the list over the past five years, although they’ve shifted positions among one another. European countries top the charts: This year, the report deems Norway the happiest country in the world, followed by Denmark, Iceland and Switzerland. Unfortunately, the U.S. has never been in the top 10, this year falling from 11th to 14th place.

Following are 10 findings about what inspires happiness worldwide.

1. Self-employed people have higher overall life evaluations.

Self-employment has its pros and cons. In developed countries, self-employed people report being more satisfied than those who are traditionally employed. However, they are also more likely to experience negative feelings such as stress and worry.

2. People are happier when they maintain their social life throughout the work week.

Most Americans feel happier on the weekends. But this weekend effect disappears when people work in a “high trust” environment -- where they consider their boss not as a superior but a partner -- and when they take part in social activities throughout the week.

Related: To Get What You Want, Be Happy First

3. American happiness has been declining over the past decade.

For the past decade, Americans have been reporting lower and lower happiness levels. They associate their decreased satisfaction with having less social support and personal freedom, lower donations and more corruption in business and government.

4. It’s important to prioritize your mental health.

Researchers conducted surveys in the U.S., Australia, Britain and Indonesia to uncover the key determinants of unhappiness and misery.

They found that receiving a diagnosis for a mental illness contributes more to a person’s happiness than income, employment or a diagnosed physical illness. “In every country physical health is of course also important, but in no country is it more important than mental health,” the report states.

While efforts to eliminate poverty and reduce unemployment are crucial, it turns out the most powerful way to decrease misery in the world is the elimination of depression and anxiety disorders (the most common forms of mental illness).

5. Have someone to count on.

People who have partners, solid relationships with extended family or simply someone to count on are much happier than those who do not. The report states that for every 10 percent of a country’s population that establishes social support, happiness levels jump by more than 20 percent.

6. Family, childhood and schooling influence happiness later in life.

For adults, happiness levels are largely influenced by one’s current economic, social and health status. While backgrounds are often overlooked, they too play a major part in determining overall life satisfaction.

7. Income is more important than education.

Survey respondents from all countries included in the report agreed that income is more important for happiness than education. Some of the wealthiest people in the world are college dropouts, including Mark Zuckerberg and Bill Gates.

Related: 11 Habits of Truly Happy People

8. Happiness depends on the type of job a person has.

It’s no surprise that employed people are happier than unemployed people, but the type of job also comes into play. The report reveals that blue-collar jobs -- in industries such as construction, mining, manufacturing, transport, farming, fishing and forestry -- tend to correlate with lower levels of happiness.

9. It’s not all about the money.

Salary isn’t the only career aspect that influences one’s happiness level. Social status at work, social relationships in the workplace, daily work structure and goal-setting also play major parts in determining happiness levels.

10. Unemployment is “scarring.”

Even after a person finds another job, the report explains, the unhappy feelings they experienced while unemployed linger.
---
http://www.getrichslowly.org/blog/2017/03/07/best-savings-accounts-for-2017/
business

---
https://hbr.org/2017/03/does-work-make-you-happy-evidence-from-the-world-happiness-report
business
A new analysis draws on data from hundreds of thousands of individuals across the globe, investigating the ways in which elements of people’s working lives drive their wellbeing. The type of job you have matters: white-collar and managerial workers are generally happier than blue-collar workers. Where you live also matters: life evaluation fluctuates according to country. Interestingly, in all countries, self-employed people report both more positive and more negative work experiences, suggesting that being your own boss is both rewarding and stressful. Being unemployed is miserable, and not just for the unemployed person: people who remain employed in areas with high unemployment also report lower overall wellbeing. Finally, while pay is important, certain non-monetary aspects of employment matter too: factors like autonomy and work-life balance can influence how happy you feel.

Since most of us spend a great deal of our lives working, it is inevitable that work plays a key role in shaping our levels of happiness. In a recent chapter of the World Happiness Report — published annually to coincide with the United Nation’s International Day of Happiness — we look more closely at the relationship between work and happiness. We draw largely upon the Gallup World Poll, which has been surveying people in over 150 countries around the world since 2006. These efforts allow us to analyze data from hundreds of thousands of individuals across the globe and investigate the ways in which elements of people’s working lives drive their wellbeing.

Subjective wellbeing – often loosely referred to as happiness – can be measured along multiple dimensions. We look primarily at how people evaluate the quality of their lives overall, something Gallup measures according to the Cantril Ladder, an 11-point scale where the top step is your best possible life and the bottom step is your worst possible life. Gallup then asks respondents to indicate which step they’re currently on. We look at this rating, and also investigate the extent to which people experience positive and negative affective states like enjoyment, stress, and worry in their day-to-day lives, as well as analyzing responses to more workplace-specific measures such as job satisfaction and employee engagement.

Which jobs are happiest?

Eleven broad job types are recorded in the Gallup World Poll. The available categories cover many kinds of jobs, including being a business owner, office worker, or manager, and working in farming, construction, mining, or transport. Which groups of workers are generally happier?

The first thing we notice is that people working blue-collar jobs report lower levels of overall happiness in every region around the world. This is the case across a variety of labor-intensive industries like construction, mining, manufacturing, transport, farming, fishing, and forestry. People around the world who categorize themselves as a manager, an executive, an official, or a professional worker evaluate the quality of their lives at a little over 6 out of 10, whereas people working in farming, fishing, or forestry evaluate their lives around 4.5 out of 10 on average.

This picture is not only found for overall life evaluation but also for the specific, day-to-day emotional experiences of workers. White-collar workers generally report experiencing more positive emotional states such as smiling, laughing, enjoyment, and fewer negative ones like feelings of worry, stress, sadness, and anger.

These descriptive statistics represent the raw differences in happiness across job types. Of course, there are likely to be many things that differ across people working in these diverse fields that could potentially be driving these happiness differentials. Perhaps surprisingly, much of the picture remains similar even once we adjust our estimates to take into account differences in income and education as well as a number of other demographic variables like age, gender, and marital status.

Self-employment is complicated

Being self-employed has a multifaceted relationship with wellbeing. When we look at global averages, we see that self-employment is generally associated with lower levels of happiness as compared to being a full-time employee. But follow-up analyses indicate that this very much depends on the region of the world that is being considered as well as which measure of subjective wellbeing is under consideration.

In most developed nations, we find that being self-employed is associated both with higher overall life evaluation and with more negative, daily emotions such as stress and worry. It will most likely come as no surprise to anyone who owns a business that being self-employed can be both rewarding and stressful!

Being unemployed is miserable

One of the most robust findings in the economics of happiness is that unemployment is destructive to people’s wellbeing. We find this is true around the world. The employed evaluate the quality of their lives much more highly on average as compared to the unemployed. Individuals who are unemployed also report around 30 percent more negative emotional experiences in their day-to-day lives.

The importance of having a job extends far beyond the salary attached to it. A large stream of research has shown that the non-monetary aspects of employment are also key drivers of people’s wellbeing. Social status, social relations, daily structure, and goals all exert a strong influence on people’s happiness.

Not only are the unemployed generally unhappier than those in work, we find in our analyses that people generally do not adapt over time to becoming unemployed. More than this, spells of unemployment also seem to have a scarring effect on people’s wellbeing, even after they have regained employment.

The experience of joblessness can be devastating to the individual in question, but it also affects those around them. Family and friends of the unemployed are typically affected, of course, but the spillover effects go even further. High levels of unemployment typically heighten people’s sense of job insecurity, and negatively affect the happiness even of those who are still in employment.

Job satisfaction around the world

So far, we have discussed how people evaluate and experience their lives as a whole. But what about more specific workplace wellbeing measures, like job satisfaction?

The Gallup World Poll asks respondents a yes/no question as to whether they are satisfied with their jobs. The percentage of respondents who reported to be “satisfied” (as opposed to “dissatisfied”) were higher in countries across North and South America, Europe, and Australia and New Zealand. Specifically, Austria takes the top spot with 95% of respondents reporting being satisfied with their jobs. Austria is followed closely by Norway and Iceland. We see a moderate correlation (0.28, where a perfect correlation would be 1.0) between job satisfaction responses and life evaluation for individuals in the Gallup World Poll.

To find out why some societies appear to generate greater job satisfaction than others, we turned to the more fine-grained data from the European Social Survey. This can give us more information on job quality by revealing particular workplace characteristics that relate to employee happiness. As might be expected, we find that people in well-paying jobs are happier and more satisfied with their lives and their jobs, but a number of other aspects of people’s jobs are also strongly predictive of varied measures of happiness.

Work-life balance emerges as a particularly strong predictor of people’s happiness. Other factors include job variety and the need to learn new things, as well the level of individual autonomy enjoyed by the employee. Moreover, job security and social capital (as measured through the support one receives from fellow workers) are also positively correlated with happiness, while jobs that involve risks to health and safety are generally associated with lower levels of wellbeing. We suspect that countries that rank high in terms of job satisfaction provide better quality jobs by catering to these non-pecuniary job characteristics.

High degrees of job satisfaction can hide low levels of engagement

The Gallup World Poll asks whether individuals feel “actively engaged,” “not engaged,” or “actively disengaged” in their jobs. In contrast to the relatively high job satisfaction numbers, these data paint a much bleaker picture. The number of people noting that they are actively engaged is typically less than 20%, while being around 10% in Western Europe, and much less still in East Asia.

The difference in the global results between job satisfaction and employee engagement may partially be attributable to measurement issues. But it also has to do with the fact that both concepts measure different aspects of happiness at work. Job satisfaction can perhaps be reduced to feeling content with one’s job, but the notion of (active) employee engagement requires individuals to be positively absorbed by their work and fully committed to advancing the organization’s interests. Increased employee engagement thus represents a more difficult hurdle to clear.

Although we’ve focused here on the role of work and employment in shaping people’s happiness, it is worth noting that the relationship between happiness and employment is a complex and dynamic interaction that runs in both directions. Indeed, an increasing body of research shows that work and employment are not only drivers of people’s happiness, but that happiness can itself help to shape job market outcomes, productivity, and even firm performance. Being happy at work thus isn’t just a personal matter; it’s also an economic one.
---
http://www.inc.com/betsy-mikel/these-lab-grown-chicken-strips-actually-sound-quite-tasty-theyve-got-just-one-me.html
business
It tastes like chicken.

That's what taste testers of a new lab-grown chicken strip said, Wall Street Journal reported.

Silicon Valley startup Memphis Meats unveiled their Southern battered-and-fried strips last week. Though grown from chicken cells, no actual animals were raised or slaughtered to produce the final product. Memphis Meats is targeting 2021 to launch the chicken and other lab-grown meat products to the public. There's just one problem, and kind of a big one at that. The price tag.

CREDIT: Photo courtesy of Memphis Meats

Right now, it costs $9,000 a pound to produce, the WSJ piece claims. The average price for conventional chicken at most American grocery stores? Just $3.22 per pound. Memphis Meats still has a long way to go to prep their chicken for public consumption. But they seem confident they can do so. In the press release announcing the new chicken, Memphis Meats says the "team expects to continue reducing production costs dramatically."

If they can find a way to reduce the cost by over 99 percent, Memphis Meats might just have a winner. Their press release reports that chicken is the most devoured protein in the United States, with Americans eating on average of 90 pounds of it a year. Chicken is a $90 billion market.

100% meat, far less environmental impact

Memphis Meats calls this frontier of food "clean meat." Though they use real animal cells, the environmental impact is far less than breeding, feeding and slaughtering chickens. "The way conventional poultry is raised creates huge problems for the environment, animal welfare, and human health," Memphis Meats co-founder and CEO Uma Valeti, M.D. said in a press release.

Encroaching on other meats

Chicken isn't the only protein Memphis Meats has successfully grown in a lab. Last year the startup announced its first product, a lab-grown meatball. At the time, it reportedly cost the company $18,000 to produce a pound of ground beef, compared to just $4 a pound for conventional beef.

They've unveiled other lab-grown poultry, too. This recent announcement included duck and succulent-looking photos of Duck à L'Orange. Memphis Meats mentions that 6 billion pounds of duck are consumed in China each year, so they're clearly targeting a global audience.

Would you eat a lab-grown chicken strip, meatball or duck breast? What would be the most you'd pay to try it?
---
http://sethgodin.typepad.com/seths_blog/2017/03/three-simple-and-difficult-steps.html
business
Poke The Box

The latest book, Poke The Box is a call to action about the initiative you're taking - in your job or in your life, and Seth once again breaks the traditional publishing model by releasing it through The Domino Project.

ONLINE:

IN STORES:
---
https://www.theatlantic.com/business/archive/2017/03/shapiro-racial-wealth/520098/?utm_source=feed
business
There’s little disagreement about the fact that economic inequality is problematic. But arguments persist over its origins, solutions, and which economic gaps are ultimately the most pernicious. In his new book, Toxic Inequality: How America's Wealth Gap Destroys Mobility, Deepens the Racial Divide, and Threatens Our Future, Tom Shapiro, a professor of law and sociology at Brandeis University, lays out how government policy and systemic racism has created vast gaps in wealth between white and black Americans. Shapiro and his colleagues followed 187 families from Boston, St. Louis, and Los Angeles. Half of the families were black and half white. They interviewed them in 1998 and then again in 2010, to see what had changed: how were their kids faring, how had they weathered the recession—were they any better off in 2010 than they had been in 1998? I spoke with Shapiro about his new book, how policy impacts racial wealth, and what he makes of current conversations about race and economic pain.

The interview below has been lightly edited for clarity. Gillian White: I think many people assume that the norm in America is economic mobility. A takeaway that I got from your book is that this is not the norm for everyone, but certainly not for black people. Is that correct? Tom Shapiro: There's a lot more up and down. What works for me is the metaphor of the ladder: If you go up a rung, it represents economic mobility, how much better you do than your parents did. The rungs of the ladder in the last thirty years have gotten further apart. So it's harder to move from one rung to another. To fall back, the rungs have gotten closer together. It's easier to fall back. The story for African Americans is a different than it is for whites. With a combination of a growing economy in the 1950s and 60s in particular, opening up of educational institutions, opening up of some fair housing and some fair lending, we really did see the development of an African American middle class—we're talking about hundreds of thousands of families. When whites do that, they are much more able to pass that earned middle-class status along to their children, because of the wealth they've built up in their homes and other things, and the schools and the educations that their kids get. African Americans don't. The fall-back of the children of African Americans who have earned that middle-class or professional status, in terms of economic mobility, is pretty great.

White: A lot of people would assume that if your parents achieved middle-class status, the thing that they will be able to bequeath to you, even if it's not dollars, is a better education. That if they move to a better house it comes with a better public-school system, or if their income increases, they’re able to spend on private schools, or maybe help with college. But that seems to not always work out right? Shapiro: The move is to a better educational system, but often times not to a significantly better educational system. Middle-class African Americans for the most part, given the high levels of continual residential segregation in the United States, don't live with their white counterparts. They may live in communities that are adjacent to the black community they move from, so there are more things that are shared with the community of professional African Americans they’ve left than with the white middle-class community. White: It’s pretty easy to think about all the ways the government has been complicit in racial economic inequality in the past: Jim Crow, housing segregation, the structure of the GI Bill. Many people look at those things and assume they’ve been taken care of, but you say that’s not necessarily true. Shapiro : I would point, for example, to Social Security. More people are beginning to understand that when Social Security was passed it intentionally excluded African Americans primarily, but also Latinos by excluding agricultural workers, domestic workers, railroad workers—those occupations where workers of color were concentrated. There's a historical interpretation that says that was very intentional in order to get Southern senators' support. That's a massive act of exclusion that pertains not just to 1934, it carries itself forward then for a generation that didn't have the same chance at retirement security or disability coverage, at all of the things that Social Security helps cover.

And even when reform measures brought in agricultural workers and others, those workers still don't have the same amount of social-security credits or occupations that were included at the beginning. History is really important here, especially when we talk about family financial wealth. White: And what about current policy? Shapiro: Current policy continues with somewhat of a blind eye to the way implicit racism is operating here. I would point to something like the mortgage-interest deduction, where we invest in the public good of housing approximately $200 billion a year. If you're a homeowner, whatever your [tax] bracket is, that percent of your interest payment you can take off your taxes. It's not so complex of a mechanism, but it operates in a way that privileges those with the highest mortgages. That is, wealthier families. It privileges those that tend to live in suburbs, and sort of actively mitigates against low- and moderate-income homeowners, and homeowners of color whose homes do not increase in value as much. Of that $200 billion that's invested, the top 10 percent of income earners gather 72 percent of that mortgage-interest deduction. White : That’s pretty significant. How can that be fixed? Shapiro : That's an easy one. We could flip that in a second by making it flat, by capping it, there's some really easy ways to do that. I think the dynamite question is: what is the racial and ethnic distributional breakdown of who is getting the mortgage-interest deduction? The IRS doesn't collect data on taxes by race, for good reason. The crude way of doing it—my estimate is that African Americans, about 13 percent of the population, bring in about 3.5 percent of the mortgage-interest deduction—I think $30 to $40 billion is left on the table compared with if there were any kind of demographic equity in the distribution. That is another thing that actively impacts wealth-building on a deeply racialized basis.

White: One of the families you follow that was living in St. Louis, Missouri. At one point they say that Ferguson served as one of their dream locations—somewhere they could move for a better life, better schools, better homes. In the wake of the Mike Brown shooting, Americans understand that Ferguson also had plenty of problems. When black families choose to move for better opportunities, how successful are they usually in moving to places where those dreams are actually realized? Shapiro: The challenges are much more severe for families of color that are trying to make that move. This is symbolic of the desire of all people, not just African Americans, to find a better community for themselves and their kids, especially safety in schools. That's what resonated the most. It's difficult to make that move. In St. Louis in particular, the history is that African Americans from the North side of the city kept moving out further to the suburbs to the Northwest, and the conditions in inner-city St. Louis simply followed them. Those were small, independent municipalities that didn't have a very good tax base, just like Ferguson, and ended up having to finance city revenues through fines and traffic tickets. It's a story that brought Michael Brown and that police officer together in one space and time, in one moment. It's policy that did that. White: At one point you reference an asset poverty line. I think by now we all understand the poverty line and know that lots of Americans of all shapes and stripes do not have adequate savings. But could you talk to me about what asset poverty is?

Shapiro: It's something that Melvin Oliver and I made up as we were writing Black Wealth, White Wealth over 20 years ago. Part of what we were doing was addressing the challenge of how society only measures inequality in terms of income, and we were trying to break that paradigm. The standard of measuring poverty in the United States is income poverty, which as we know is something from the mid-1960s. It's typical market basket times three, because we think the market basket is a third of a family's expenses, or at least they did in 1964. That's the standard. Literally, the Department of Agriculture goes shopping every month and gets that standard and measures it. So we asked ourselves the question: What if, to complement that income standard of what poverty is in the United States, we had something about asset or wealth poverty?



We defined asset or wealth poverty as a family that didn't have three months in wealth, of the equivalent of three months of income at the poverty line. So it's a relatively low level: What the government defines as the income poverty line for a family of two, or three, or four, we take that and then multiply that month by three. If we were to do it today, we probably would have done six. For other organizations like CFED, it's become semi-standard, the three-month marker. And its a pretty cautious or conservative one. You can justify it pretty easily. That's what asset poverty is.
---
http://www.economist.com/news/world-week/21718933-business-week
business
Following heavy hints that it would do so, the Federal Reserve lifted the range for its benchmark interest rate by a quarter of a percentage point to between 0.75% and 1%, and said there would be more rises to come this year. Solid jobs data sealed the decision for Fed officials. Employers created 235,000 jobs last month; wages were up by 2.8%.

Super Mario

The European Central Bank tinkered with the guidance it issues at its policy meeting, which markets interpreted as a signal that it was pondering a pull-back on quantitative easing. Mario Draghi, the ECB’s president, said the bank no longer had a “sense of urgency” to take more action on stimulus because the battle against deflation had been won. But any increase in interest rates is not likely to happen until next year.

After just two weeks in the job, Charlotte Hogg resigned as a deputy governor of the Bank of England for not revealing that her brother is a senior executive at Barclays, a potential conflict of interest. An initial offer to step down by Ms Hogg was rejected by the governor, Mark Carney, but a damning report on the matter by a committee in Parliament made her position untenable.

Four men were charged in America with hacking 500m Yahoo accounts in 2014, one of the biggest breaches of internet security to date. Two of the men are agents of Russia’s intelligence service. They are accused of conspiring with the other two men, one of whom is on the list of the FBI’s most-wanted cyber-criminals.

Donald Trump nominated Chris Giancarlo as chairman of the Commodity Futures Trading Commission . The CFTC regulates the $700trn derivatives market. Mr Giancarlo supports the broad thrust of the Dodd-Frank reforms, though he has been critical of certain aspects of the law and has opposed tighter regulations for high-frequency trading firms.

Hancock’s last hour

American International Group started the search for a new chief executive—its seventh since 2005—following the resignation of Peter Hancock in the wake of a bigger-than-expected quarterly loss.

Oil prices fell by 10% over a week, dropping to where they were before OPEC agreed to curtail production (in order to boost prices) late last year. A build-up of American crude supplies fed concerns that the oil glut will not ease soon.

Anil Agarwal, an Indian mining tycoon, revealed plans to buy shares worth $2.4bn in Anglo American , making him its second-biggest shareholder. Last year Mr Agarwal tried and failed to engineer a merger of his mining group with Anglo. He insists his latest move is just a family investment.

Last year’s rally in commodity prices helped to push Antofagasta’s annual headline profit up by 79%, to $1.6bn. The Chilean copper-mining group reckons that a rebound in demand from China and tighter supply because of the scarcity of new supplies will keep copper prices buoyant.

The scandal in South Korea that has led to the removal of the country’s president and charges being laid against the de facto head of Samsung spread to SK Group , as prosecutors questioned three people with links to the chaebol .

The Musk challenge

Elon Musk offered to solve an energy crisis in South Australia that has led to blackouts. Prior to talks with the government, the founder of Tesla and SpaceX said he could install a battery-storage system that connects to the grid within 100 days, and would not charge for the project if he failed to meet his deadline.

EON , a German utility, registered an annual net loss of €16bn ($18bn) because of costs associated with spinning off its fossil-fuel assets and funding the storage of nuclear waste. EON noted that the loss meant it was “freed from past burdens”, leaving it to focus on its business in networks, consumer retail and renewables.

With its core chipmaking business slowing down, Intel accelerated its drive into the market for autonomous cars by agreeing to pay $15.3bn for Mobileye, an Israeli company. Mobileye’s systems enable autonomous cars to recognise pedestrians, traffic and road signs, though last year it had a very public falling out with Tesla after one of the electric-carmaker’s vehicles was involved in a fatal crash. See article .

Iceland withdrew the last of the capital controls it imposed when its banking industry imploded during the financial crash in 2008. The krona recorded its biggest one-day decline in eight years after the lifting of capital controls was announced. A surge in tourism has bolstered GDP, which grew by 11.3% in the fourth quarter of 2016, prompting some to fret that Iceland’s economy is now overheating. See article .