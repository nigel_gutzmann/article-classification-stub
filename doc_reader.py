import random


class Article(object):
    """
    You may manipulate this class if you'd like. It just stores the article data
    as a python object.
    """

    def __init__(self, url, article, category):
        self.url = url
        self.article = article
        self.category = category


class Reader(object):
    """
    This class should not be changed. It reads the toy dataset so that you can
    use it. Here's how it should be called:

    from doc_reader import Reader
    reader = Reader(data_source='train')
    article_list = reader.read()

    Now article_list will be a list of all of the available Article objects.

    The data_source parameter can be one of:
        - 'train': to get a larger list of pre-categorized documents
        - 'eval': to get a smaller list of pre-categorized documents
        - 'test': to get a small list of un-categorized articles (their
                  categories will be "unknown")
    """

    data_sources = {
        'train': [
            'data/entertainment.txt',
            'data/business.txt',
            'data/news.txt',
            'data/sports.txt',
        ],
        'eval': ['data/knowns.txt', ],
        'test': ['data/unknown.txt', ],
    }

    def __init__(self, data_source='train'):
        if data_source not in ['train', 'eval', 'test']:
            raise Exception(
                'You must supply either "train",'
                ' "eval" or "test" as the data_source'
            )

        self.file_list = self.data_sources[data_source]

    def read(self):
        article_list = []
        for filename in self.file_list:
            article_list.extend(
                self._read_file(filename))

        random.shuffle(article_list)
        return article_list

    def _read_file(self, filename):
        file_articles = []
        with open(filename) as open_file:
            file_contents = open_file.read()
            article_data = file_contents.split('---')
            for data in article_data:
                file_articles.append(
                    self._build_article(data.strip()))
        return file_articles

    def _build_article(self, article_data):
        split_article_data = article_data.split('\n')
        article_url = split_article_data[0]
        article_category = split_article_data[1]
        article_text = '\n'.join(split_article_data[2:])
        return Article(
            url=article_url, article=article_text, category=article_category)
