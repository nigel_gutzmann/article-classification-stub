from doc_reader import Reader

train_reader = Reader(data_source="train")
training_articles = train_reader.read()

eval_reader = Reader(data_source="eval")
evaluation_articles = eval_reader.read()

test_reader = Reader(data_source="test")
test_articles = test_reader.read()

print "Found " + str(len(training_articles)) + " training articles"
print "Found " + str(len(evaluation_articles)) + " evaluation articles"
print "Found " + str(len(test_articles)) + " test articles"
